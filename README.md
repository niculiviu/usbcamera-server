## Project description
The project main purpose is related to video live streaming. The overall architecture is:

              ________________                   __________________    USB Ports            __
             | Vlc client     |                 |                  |-------------> USBs    |. |
             |     or         |  <---------->   | Raspberry Pi3B   |-------------> Video   |. |
             | other apps with|                 |                  |------------->
             | RTSP           |                 | (RTSP server)    |.............  Cameras |. |
             |________________|                 |__________________|------------->         |__|


## Description
This app uses LIVE555 Streaming Media library(http://www.live555.com) in order to initialize an RTSP server and
creates a ServerMediaSession. live555 was chosen because is well suited for embedded device. The other open source
streaming libraries(e.g gstreamer, ffserver, etc) have a lot of dependencies and hence the overhead.
Other libraries used are:
 * libusb, to get access to the properties of the usb device that was attached.
 * libevent, used for it's callback mechanism when an usb device is inserted. A callback function is called when
   usb is inserted.
 * x264 encoder, used when we get raw data from the usb camera device. In order to pass the data to the live555 api's
   this data must be encoded in suitable format.
 * libswscale, used also for encoding.

Note: given the sps and pps issues testing is being done on both x264 and swscale encoders.

## Dependencies

this app depends on:
  
* URI: http://www.live555.com
* URI: https://github.com/libusb/libusb
* URI: https://libevent.org
* URI: https://www.videolan.org/developers/x264.html

TODO: add instructions on how to build the dependencies

## Build
```
usbcamera-server$ make
```

## Note
Current status is that a few hundred frames are being received from this app. Even though the sps and
the pps are being hardcoded, the vlc client is not getting them after some time hence the vlc
decoder cannot continue.
