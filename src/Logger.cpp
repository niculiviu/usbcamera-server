#include "Logger.h"

const int Logger::DEBUG = LOG_DEBUG;
const int Logger::INFO  = LOG_INFO;
const int Logger::ERROR = LOG_ERR;


Logger::Logger(const char *logName)
{
	openlog (logName, (LOG_CONS | LOG_PID | LOG_NDELAY), LOG_USER);
}

Logger::~Logger()
{
	closelog ();
	printf("\nDestructor Logger\n");
}

void Logger::log(const int logPriority, const char *message, ...)
{
	syslog (logPriority, "%s", message);
}

