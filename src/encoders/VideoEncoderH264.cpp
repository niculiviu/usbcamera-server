#include "encoders/VideoEncoderH264.h"
#include "V4l2LiveSource.h"

#define MAX_PLANES_PER_PICTURE 4

VideoEncoderH264::VideoEncoderH264() : encoder(nullptr), fps(25), nals(nullptr), piNal(0),
					inFrame(nullptr), outFrame(nullptr), pFile(nullptr),
					inPts(0), outPts(0), dts(0)
{
    int width = 640, height = 480;
    
    x264_param_default_preset(&mParam, "ultrafast", "zerolatency");

    /* Configure non-default params */
    mParam.i_bitdepth = 8;
    mParam.i_csp = X264_CSP_I422;
    mParam.i_width  = width;
    mParam.i_height = height;
    mParam.b_vfr_input = 0;
    mParam.b_repeat_headers = 1;
    mParam.b_annexb = 1;
    mParam.i_bframe = 0;
    mParam.i_threads = 1;
    mParam.i_fps_num = fps;
    mParam.i_fps_den = 1;

    //mParam.b_intra_refresh = 1; //periodic intra refresh instead of IDR frames , still freezes
 //but no sps and pps warnings

    mParam.i_log_level = X264_LOG_DEBUG;

    encoder = x264_encoder_open(&mParam);

    if (!encoder) {
	printf("could not create x264 encoder\n");
	throw runtime_error("Could not create X264 encoder!\n");
    } else {
	x264_picture_init(&picIn);
	x264_picture_init(&picOut);
	if(x264_picture_alloc(&picIn, mParam.i_csp, width, height) < 0 ) {
	    printf("failed to allocate data for picture\n");
	    // exception
	    picIn.i_type 	= X264_TYPE_AUTO;
	    picIn.img.i_plane	= 3;
	    picIn.img.i_csp  	= mParam.i_csp;
	}
    }

    inFrame = av_frame_alloc();
    outFrame = av_frame_alloc();

    int nheader = 0;
    int header_size = 0;

    int outWidth = width;
    int outHeight = height;
    out_pixel_format = AV_PIX_FMT_YUV422P;
    in_pixel_format = AV_PIX_FMT_YUYV422;
    sws = sws_getContext(width, height, in_pixel_format, outWidth, outHeight, out_pixel_format,
			SWS_FAST_BILINEAR, NULL, NULL, NULL);
    if (!sws) {
	printf("VideoEncoderH264: cannot create sws context\n");
    }

    pFile = fopen("/home/nicu/work/raspberryPi/live.2019.08.28/testProgs/test.264", "w+b");
    if (!pFile) {
	printf("VideoEncoderH264: cannot open h264 dest file\n");
    }

    /*header_size = x264_encoder_headers(encoder, &nals, &nheader);
    if (header_size < 0) {
	printf("VideoEncoderH264: encoder headers failed!\n");
    }

    //header_size = nals[0].i_payload + nals[1].i_payload ;//+ nals[2].i_payload;
    /*printf("nal[0] payload %x\n", nals[0].p_payload);
    for(int i = 0; i < header_size; i++){
	printf("nal[0][%d]= %x\n", i, nals[0].p_payload[i]);
    }
    if (!fwrite(nals[0].p_payload, header_size, 1, pFile)) {
	printf("VideoEncoderH264: cannot write headers\n");
    }
*/
    inPts = 0;
    
}

VideoEncoderH264::~VideoEncoderH264()
{
    printf("~VideoEncoderH264\n");
    if (encoder) {
	x264_encoder_close(encoder);
	//I get crash on this ???
	//x264_picture_clean(&picIn);
	encoder = nullptr;
    }

    if (sws) {
	sws_freeContext(sws);
	sws = nullptr;	
    }

    if (pFile) {
	fclose(pFile);
	pFile = nullptr;
    }

    av_free(inFrame);
    av_free(outFrame);
}

bool VideoEncoderH264::encodeHeadersFrame()
{
    
}

EncodedVideoFrame *VideoEncoderH264::encodeFrame(unsigned char *rawFrame, unsigned int rawFrameSize)
{

    //sliceType ?
/*
switch (type) {
    case 0:
	en->picture->i_type = X264_TYPE_P;
	break;
    ...	X264_TYPE_IDR
    ... X264_TYPE_I
    defult:
    X264_TYPE_AUTO
......
*/
    /*static bool forceIntra = false;
    if(!forceIntra) {
	picIn.i_type = X264_TYPE_IDR;
	forceIntra = true;
    } else {
	picIn.i_type = X264_TYPE_AUTO;
    }*/


    EncodedVideoFrame *encodedFrame = new EncodedVideoFrame();

    int width = 640, height = 480;
    int ret;

    //copy raw data into our raw input container
    if (av_image_fill_arrays(inFrame->data, inFrame->linesize, rawFrame, AV_PIX_FMT_YUYV422, 
	    width, height, 1) <= 0) {
	printf("VideoEncoderH264: Could not feed AVFrame\n");
    }
    inFrame->width = width;
    inFrame->height = height;
    inFrame->format = AV_PIX_FMT_YUYV422;

    /*
    for(int i = 0; i < MAX_PLANES_PER_PICTURE; i++) {
	picIn.img.i_stride[i] = inFrame->linesize;
	picIn.img.plane[i] = inFrame->data;
    }*/

    //convert to I420 for x264
    int h = sws_scale(sws, inFrame->data, inFrame->linesize, 0, inFrame->height, 
		picIn.img.plane, picIn.img.i_stride);
    if (h <= 0) {
	printf("VideoEncoderH264: Could not convert image\n");
    }


    picIn.i_pts = inPts;
    ret = x264_encoder_encode(encoder, &nals, &piNal, &picIn, &picOut);
    if (ret < 0){
	printf("VideoEncoderH264: could not encode video frame!\n");
	return nullptr;
    }
    
    if (!nals){
	printf("VideoEncoderH264:x264 encoder encode failed!\n");
    }

    inPts++;

    /*if (!fwrite(nals[0].p_payload, ret, 1, pFile)) {
	printf("VideoEncoderH264: Could not write nal\n");
    }*/

    if (ret >= 0) {
	/*static bool finished = false;
	if(!finished) {
	int header_size = x264_encoder_headers(encoder, &nals, &piNal);
	    if (header_size < 0) {
		printf("VideoEncoderH264: encoder headers failed!\n");
	    }	    
	    finished = true;
	}*/

        outPts = picOut.i_pts;
        dts = picOut.i_dts;

        for (int i = 0; i < piNal; i++) {	
    	    printf("VideoEncoderH264:nals[%d].i_payload=%d\n", i, nals[i].i_payload);
	    bool success = encodedFrame->setNal(nals[i].p_payload, nals[i].i_payload);
	    if (!success) {
		printf("VideoEncoderH264: null\n");
		return nullptr;
	    }
	}

    }

    return encodedFrame;
}
