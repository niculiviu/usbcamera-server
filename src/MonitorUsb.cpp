#include "MonitorUsb.h"
#include "CameraUsbDevice.h"
#include "Util.h"
#include "Logger.h"


int MonitorUsb::mSock = 0;
vector<CameraUsbDevice *> *MonitorUsb::mPtrDevicesList = nullptr;
bufferevent * MonitorUsb::mBufevent	= nullptr;

static Logger l("MonitorUsb");

MonitorUsb::MonitorUsb(vector<CameraUsbDevice *> *dev)
{
    mPtrDevicesList = dev;
}

int MonitorUsb::startMonitor(void)
{
    struct sockaddr_nl sa;

    struct event_base *base;

    base = event_base_new();
    if (!base) {
        l.log(l.ERROR, "Could not initialize libevent!\n");
    }

    memset(&sa, 0,sizeof(sa));
    sa.nl_family = AF_NETLINK;
    sa.nl_pid    = getpid();
    sa.nl_groups = 0xffffffff;
    
    
    mSock = socket(AF_NETLINK, SOCK_DGRAM, NETLINK_KOBJECT_UEVENT);
    if (mSock < 0) {
        l.log(l.ERROR, "Error creating uevent socket :%s\n", strerror(errno));
    }

    int res;
    res = bind(mSock, (struct sockaddr *) &sa, sizeof(sa));
    if (res < 0) {
        l.log(l.ERROR, "Error binding socket: %s\n", strerror(errno));
    }


    mBufevent = bufferevent_socket_new(base, mSock, BEV_OPT_CLOSE_ON_FREE);
    bufferevent_setcb(mBufevent, readcb, NULL, eventcb, NULL);
    bufferevent_enable(mBufevent, EV_READ);

    event_base_dispatch(base);

    event_base_free(base);

    return 1;
}

int MonitorUsb::readUsbAttributes(char *output, char *attribute, char *sysPath)
{
    char buff[100] 	= {0};
    char full_path[256]	= {0};
    sprintf(full_path, "/sys%s/%s", sysPath, attribute);

    FILE * f = fopen(full_path,"r");
    if (f) {
	if ((fread(buff, 100, 1, f) > 0) || (feof(f))) {
	    strcpy(output, buff);
	}
	fclose(f);
	return 1;
    }
    return -1;
}

void MonitorUsb::readcb(struct bufferevent *bev, void *ctx)
{
    char *uevent_path		= nullptr;
    int mode			= -1;
    struct evbuffer *inputbuf 	= bufferevent_get_input(bev);
    int buflen = (int)evbuffer_get_length(inputbuf);
    char *data = (char *) malloc(buflen);
    evbuffer_remove(inputbuf, data, buflen);

    //printf("data: %s\n", data);
	
    if (!strncmp("add@", data, 4)) {
	uevent_path = data + 4;
	mode = 1;
    } else if (!strncmp("remove@", data, 7)) {
	uevent_path = data + 7;
	mode = 0;
    }

    /* device is added */
    if (mode == 1) {
	char v4lDevice[20] = {0};
	Util::getV4l2Device(uevent_path, v4lDevice);

	/* if this is a video4linux device */
	if (strlen(v4lDevice) > 0) {
	    __u32 caps;
	    bool isVideoCamera = Util::isVideoCaptureDevice(v4lDevice, &caps);
	    if (isVideoCamera)
	    {
		//TODO: check if allocation fails e.g new (nothrow)
		// check pointer after deletion with delete. assign it to nullptr?
		CameraUsbDevice *cameraObject = Util::getCameraInfo(uevent_path, v4lDevice, caps);
		mPtrDevicesList->push_back(cameraObject);
		raise(SIGUSR1);
	    }
	}
    /* device was removed */
    } else if (mode == 0) {
	//unsigned int i = 0;
	char v4lDevice[20] = {0};
	Util::getV4l2Device(uevent_path, v4lDevice);

	if (strlen(v4lDevice) > 0) {
	    
	    for(auto iter = mPtrDevicesList->cbegin(); iter != mPtrDevicesList->cend(); ++iter/*auto const &obj : *mPtrDevicesList*/) {
		const CameraUsbDevice *obj = *iter;
		if(!obj)
		    continue;

		char device[20] = {0};
		obj->getDevicePath(device);
		if(strcmp(device, v4lDevice) == 0) {
		    delete obj;
		    /* delete pointer from the list */
		     mPtrDevicesList->erase(iter);
		}
		//i++;
	    }
	}
	if (mPtrDevicesList->empty()) {
	    printf("DevicesList is empty\n");
	}
    }

    free(data);
}


void MonitorUsb::eventcb(struct bufferevent *bev, short events, void *ctx)
{
    if (events & BEV_EVENT_EOF) {
        l.log(l.DEBUG, "Connection closed.\n");
    } else if (events & BEV_EVENT_ERROR) {
        l.log(l.DEBUG, "Got an error on the connection: %s\n",
            strerror(errno));
    }

    bufferevent_free(bev);
}

MonitorUsb::~MonitorUsb()
{
    printf("\nDestructor MonitorUsb\n");
}
