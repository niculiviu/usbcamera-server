#include "Logger.h"
#include "VideoFrame.h"

VideoFrame::VideoFrame()
{
}

VideoFrame::VideoFrame(unsigned int frameSize) : mTimeStamp((struct timeval){ 0 })
{
    mFrameSize = frameSize;
    mFrameBuff = new unsigned char[frameSize];
}

VideoFrame::~VideoFrame()
{
    printf("~VideoFrame\n");
    if(mFrameBuff) {
	delete [] mFrameBuff;
    }
}

Nal::Nal() : nalPayload(nullptr), nalPayloadSize(0)
{

}

EncodedVideoFrame::~EncodedVideoFrame()
{
    printf("~EncodedVideoFrame\n");
}

EncodedVideoFrame::EncodedVideoFrame() : currentNal(0)
{

}

bool EncodedVideoFrame::setNal(unsigned char *nal, unsigned int nalSize)
{
    if(currentNal >= MAX_NALS) {
	return false;
    }

    nals[currentNal].setNal(nal);
    nals[currentNal].setNalSize(nalSize);

    currentNal++;

    return true;
}

