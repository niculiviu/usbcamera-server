#include "H264ServerMediaSubsession.h"


H264ServerMediaSubsession *H264ServerMediaSubsession::createNew(UsageEnvironment &env, StreamReplicator *replica,
		    Boolean reuseFirstSource) 
{
    return new H264ServerMediaSubsession(env, replica, reuseFirstSource);
}

H264ServerMediaSubsession::H264ServerMediaSubsession(UsageEnvironment &env, StreamReplicator *replica,
					Boolean reuseFirstSource) : OnDemandServerMediaSubsession(env, reuseFirstSource), replicator(replica)
{
}

H264ServerMediaSubsession::~H264ServerMediaSubsession()
{
}

FramedSource *H264ServerMediaSubsession::createNewStreamSource(unsigned int clientSessionId, unsigned int &estBitrate)
{
    estBitrate = 2000; //estimate kps
    
    return H264VideoStreamDiscreteFramer::createNew(envir(), replicator->createStreamReplica());
}

RTPSink *H264ServerMediaSubsession::createNewRTPSink(Groupsock * rtpGroupsock, unsigned char rtpPayloadformat,
						    FramedSource *inputSource)
{
    const u_int8_t sps[] = {0x00, 0x00, 0x00, 0x01, 0x67, 0x7a, 0x00, 0x1e, 0xbc, 
                            0xb4, 0x05, 0x01, 0xed, 0x08, 0x00, 0x00, 0x03, 0x00, 
                            0x08, 0x00, 0x00, 0x03, 0x01, 0x94, 0x78, 0xb1, 0x75
                        };
    const u_int8_t pps[] = {0x00, 0x00, 0x00, 0x01, 0x68, 0xce, 0x0f, 0xc8};

    return H264VideoRTPSink::createNew(envir(), rtpGroupsock, rtpPayloadformat, sps, 27, pps, 8);
}
