#include "V4l2Control.h"
#include "Logger.h"


V4l2Control::V4l2Control(const char *device)
{
    buffers = NULL;
    mFd = open(device, O_RDWR | O_NONBLOCK);

    if (mFd == -1) {
	throw runtime_error("Cannot initialize V4l2Control!\n");
    }
}

bool V4l2Control::configure(/**/)
{
    bool res = true;
	
    if(!setVideoFormat()) {
	printf("Canot set video format!");
	res = false;
    }
	
    if(!initMap()) {
	printf("Cannot initialize mmap!");
	res = false;
    }

    if(!startCapturing()) {
	printf("Cannot start capturing!");
	res = false;
    }	

    return res;
}

unsigned int V4l2Control::getV4l2Capabilities(void)
{
    unsigned int devCaps = -1;
    struct v4l2_capability caps;

    memset(&caps, 0, sizeof(caps));

    if (-1 == ioctl(mFd, VIDIOC_QUERYCAP, &caps)) {
	printf("cannot get device capability. No V4L2 device\n");
    } else {
	/* only device capabilities field */
	if (caps.capabilities & V4L2_CAP_DEVICE_CAPS) {
	    devCaps = caps.device_caps;
	}
    }
    return devCaps;
}

v4l2_fmtdesc V4l2Control::getVideoFormats(__u32 type)
{
    struct v4l2_fmtdesc fmt;

    memset(&fmt, 0, sizeof(fmt));
    fmt.type = type;

    while (ioctl(mFd, VIDIOC_ENUM_FMT, &fmt) >= 0);

    return fmt;
}

bool V4l2Control::setVideoFormat(void)
{
    /* set the video format(MPEG, RGB, YUV) */
    memset(&mFmt, 0, sizeof(mFmt));
    mFmt.fmt.pix.width           = 640;
    mFmt.fmt.pix.height          = 480;
    mFmt.type                    = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    mFmt.fmt.pix.pixelformat     = V4L2_PIX_FMT_YUYV;
    mFmt.fmt.pix.field           = V4L2_FIELD_INTERLACED;

    if(ioctl(mFd, VIDIOC_S_FMT, &mFmt) == -1){
        printf("Error on VIDIOC_S_FMT\n");
	return false;
    }

    return true;
}

bool V4l2Control::initMap(void)
{
    /* inform the device about the future buffers.
     * how they are allocated, and how many are.
    */
    bool res = true;
    struct v4l2_requestbuffers req;
    memset(&req, 0, sizeof(req));

    req.count   = 4;
    req.type    = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory  = V4L2_MEMORY_MMAP;

    if(-1 == ioctl(mFd, VIDIOC_REQBUFS, &req)){
        printf("Error on VIDIOC_REQBUFS,\n");
	res = false;
    }

    buffers =  new buffer[req.count]();

    if(!buffers){
        printf("Out of memory\n");
	res = false;
    }

    /*
    * allocate the buffers
    * ask the device about the amount of memory it needs and allocate it
    */
    for(mNrBuffers = 0; mNrBuffers < req.count; ++mNrBuffers) {
        struct v4l2_buffer buf;

        memset(&buf, 0, sizeof(buf));

        buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory      = V4L2_MEMORY_MMAP;
        buf.index       = mNrBuffers;

        if(-1 == ioctl(mFd, VIDIOC_QUERYBUF, &buf)){
            printf("Error on VIDIOC_QUERYBUF\n");
	    res = false;
            break;
        }

        buffers[mNrBuffers].length = buf.length;
        buffers[mNrBuffers].data   =
                                mmap(NULL,/*start anywhere*/
                                buf.length,
                                PROT_READ | PROT_WRITE /* required */,
                                MAP_SHARED /* recommended */,
                                mFd , buf.m.offset);
        if(MAP_FAILED == buffers[mNrBuffers].data) {
            printf("mmap failed\n");
	    res = false;
            break;
        }
    }

    return res;
}

unsigned int V4l2Control::getBufferImageSize()
{
    return mFmt.fmt.pix.sizeimage;
}

unsigned int V4l2Control::getBufferBytesUsed()
{
    return mBytesUsed;
}

bool V4l2Control::getFrame(unsigned char *frame)
{
    fd_set fds;
    struct timeval tv;
    int r;

    FD_ZERO(&fds);
    FD_SET(mFd, &fds);

    /* Timeout. */
    tv.tv_sec = 2;
    tv.tv_usec = 0;

    r = select(mFd + 1, &fds, NULL, NULL, &tv);

    if (r < 0) {
	printf("select error\n");
	return false;
    }

    if (0 == r) {
	printf("select timeout\n");
	return false;
    }

    return readFrame(frame);
}
//TODO: delete getBufferImageSize and get the imageSize through function parameter:
//e.g readFrame(unsigned char *frame, unsigned int *imageSize)
//*imageSize=buf.bytesused
bool V4l2Control::readFrame(unsigned char *frame /*, unsigned int *imageSize*/)
{
    struct v4l2_buffer buf;

    memset(&buf, 0, sizeof(buf));

    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;


    if (-1 == ioctl(mFd, VIDIOC_DQBUF, &buf)) {
        printf("Error on VIDIOC_DQBUF\n");
	return false;
    }

    /* data to be sent(buffers[buf.index].data, buf.bytesused fmt.fmt.pix.sizeimage)*/
    memcpy(frame, buffers[buf.index].data, mFmt.fmt.pix.sizeimage);
    mBytesUsed = buf.bytesused;
    //*imageSize=buf.bytesused

    if (-1 == ioctl(mFd, VIDIOC_QBUF, &buf)) {
        printf("Error on VIDIOC_QBUF\n");
	return false;
    }

    return true;
}

bool V4l2Control::startCapturing()
{
    unsigned int i;
    bool res = true;
    enum v4l2_buf_type type;

    for(i = 0; i < mNrBuffers; ++i){
        struct v4l2_buffer buf;

        memset(&buf, 0, sizeof(buf));

        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        buf.index = i;

        /* put the buffer into the incominq queue
         * hand the buffer over to the device and wait for it to be filled with data
	*/
        if(-1 == ioctl(mFd, VIDIOC_QBUF, &buf)){
            printf("Error on VIDIOC_QBUF\n");
	    res = false;
	}

        /* activate streaming */
        type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        if(-1 == ioctl(mFd, VIDIOC_STREAMON, &type)){
            printf("error on VIDIOC_STREAMON\n");
	    res = false;
        }
    }

    return res;
}

bool V4l2Control::stopCapturing(void)
{
    enum v4l2_buf_type type;
    type=V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if(-1 == ioctl(mFd, VIDIOC_STREAMOFF, &type)){
        printf("Error on VIDIOC_STREAMOFF\n");
	return false;
    }
    return true;
}

int V4l2Control::getV4l2Fd(void)
{
    return mFd;
}

V4l2Control::~V4l2Control()
{
    printf("V4l2Control destructor\n");

    unsigned int i;
    //TODO: check state
    stopCapturing();
    
    if(buffers) {
        delete [] buffers;
    }

    if(mNrBuffers > 0) {
        for (i = 0; i < mNrBuffers; ++i)
	    if (-1 == munmap(buffers[i].data, buffers[i].length))
		printf("error when trying to munmap!\n");
    }

    if (close(mFd) == -1)
	printf("Error closing device!");
}
