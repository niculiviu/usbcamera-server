#include "ThreadPool.h"
#include "Logger.h"

TaskQueue::TaskQueue()
{
    resetIterator();
}

void TaskQueue::resetIterator()
{
    iter = queue.begin();
}

void TaskQueue::enqueue(Runnable *r)
{
    queue.push_back(r);
    resetIterator();
}

void TaskQueue::dequeue()
{
    if(!queue.empty()) {
	queue.pop_front();
	resetIterator();
    }
}

bool TaskQueue::isEmpty()
{
    return queue.empty();
}

void TaskQueue::clear()
{
    queue.clear();
}

Runnable* TaskQueue::getCurrent()
{
    if(iter != queue.end()) {
	return *iter;
    }
    return nullptr;
}

void TaskQueue::next()
{
    if(iter != queue.end()) {
	iter++;
    }
}

ThreadPool::ThreadPool(unsigned int threads) : run(true)
{
    for(unsigned int i = 0; i < threads; i++) {
	workers.push_back(
		    thread([i, this] {worker_thread(i);})
		);
    }
}

ThreadPool::~ThreadPool()
{
    stop();
}

void ThreadPool::stop()
{
    run = false;
    cv.notify_all();
    for(thread& worker : workers) {
	if(worker.joinable()) {
	    worker.join();
	}
    }

    mQueue.clear();
}

void ThreadPool::addTask(Runnable *const task)
{
    lock_guard<mutex> lock(m_mutex);
    mQueue.enqueue(task);
    cv.notify_one();
}

void ThreadPool::removeTask()
{
    //TODO: which id?
    lock_guard<mutex> lock(m_mutex);
    mQueue.dequeue();
    cv.notify_one();
}

void ThreadPool::worker_thread(unsigned int i)
{
    Runnable *job = nullptr;

    while(true) {
	printf("%d wait\n", i);

	unique_lock<mutex> lock(m_mutex);

	/* wait untill there is something to work */
	cv.wait(lock, [this]{return !mQueue.isEmpty();});

	mQueue.resetIterator();
	job = mQueue.getCurrent();
	printf("t%d got job\n", i);
	mQueue.dequeue();

	if(!run) {
	    printf("break\n");
	    break;
	}

	//submit the work
	lock.unlock();
	cv.notify_one();
	job->setRunning(true);
	job->job(i);
    }
}
