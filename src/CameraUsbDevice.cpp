#include "CameraUsbDevice.h"
#include "Logger.h"

static Logger l("CameraUsbDevice");

CameraUsbDevice::CameraUsbDevice(const char *devicePath, char *idProduct, int busnum, char *idVendor,
                int speed, char *version, char *manufacturer, __u32 &device_capabilities)
{

    mBusNum	= busnum;
    mSpeed	= speed;
    mDeviceCap	= device_capabilities;
    if (devicePath)
        strcpy(mDevicePath, devicePath);
    if (idProduct)
        strcpy(mIdProduct, idProduct);
    if (version)
        strcpy(mVersion, version);
    if (idVendor)
        strcpy(mIdVendor, idVendor);
    if (manufacturer)
        strcpy(mManufacturer, manufacturer);
}

void CameraUsbDevice::getDevicePath(char *out) const
{
    if (out) {
	strcpy(out, mDevicePath);
    }
}

void CameraUsbDevice::getIdProduct(char *out) const
{
    if (out) {
	strcpy(out, mIdProduct);
    }
}

int CameraUsbDevice::getBusNum(void) const
{

    return mBusNum;
}

void CameraUsbDevice::getIdVendor(char *out) const
{
    if (out) {
	strcpy(out, mIdVendor);
    }
}

int CameraUsbDevice::getSpeed(void) const
{
    return mSpeed;
}

void CameraUsbDevice::getVersion(char *vers_out) const
{
    if (vers_out) {
	strcpy(vers_out, mVersion);
    }
}

void CameraUsbDevice::getManufacturer(char *man_out) const
{
    if (man_out) {
	strcpy(man_out, mManufacturer);
    }
}

__u32 CameraUsbDevice::getDeviceCapabilities(void)
{
    return mDeviceCap;
}
void CameraUsbDevice::getCameraUsbDeviceList(list<CameraUsbDevice *> &list_out) const
{

}

CameraUsbDevice::~CameraUsbDevice()
{
    printf("Destructor CameraUsbDevice\n");
}