#include "Runnable.h"
#include "Logger.h"
#include "V4l2Control.h"
#include "V4l2LiveSource.h"

//???
#include <unistd.h>

Runnable::Runnable(TaskScheduler *scheduler, V4l2LiveSource *liveSource)
{
    // add here v4l2 control configure parameters from main cmd line 
    // or add smth like setV4l2 parameters method .....
    mScheduler	= scheduler;
    mLiveSource	= liveSource;
}

Runnable::~Runnable()
{
    mStartJob = false;
}

void Runnable::job(unsigned int id)
{

    printf("Runnable:id = %d\n", id);
    unsigned int x = 10;

    try {
	V4l2Control v("/dev/video0");
	VideoEncoderH264 encoder;

	bool isConfigured = v.configure();

	if (isConfigured) {

    	    while(x-- > 0/*mStartJob*/) {
		
		unsigned int bufImageSize = v.getBufferImageSize();
    		printf("Runnable:gotRawFrame: bufImageSize:%d\n", bufImageSize);

		unsigned char rawFrame[bufImageSize];
		v.getFrame(rawFrame);

		unsigned int bufBytesUsed = v.getBufferBytesUsed();
		printf("Runnable:bytsUsed:%d\n", bufBytesUsed);

		timeval tv;
		gettimeofday(&tv, NULL);

		std::unique_lock<mutex> l(mtx);

		//get encoded frame
		EncodedVideoFrame *encodedFrame = encoder.encodeFrame(rawFrame, bufBytesUsed);
		if(!encodedFrame) {
		    printf("Runnable:Could not encode frame!\n");
		    return;
		} else {
		    int totalNals = encodedFrame->getTotalNals();
		    printf("Runnable:totalNals: %d\n", totalNals);
		    for(int i = 0; i < totalNals; i++) {
			unsigned int nalSize = encodedFrame->getNals()[i].getNalSize();
			VideoFrame *frame = new VideoFrame(nalSize);
			memcpy(frame->getFrameData(), encodedFrame->getNals()[i].getNal(), nalSize);
			V4l2LiveSource::sVideoFramesQueue.push_back(frame);
			mLiveSource->setConsumed(false);
			V4l2LiveSource::signalNewFrame(mScheduler, mLiveSource);
			//wait till frame gets processed
			//printf("Runnable:wait till frame %d gets processed!\n", i);
			//while(!mLiveSource->wasConsumed());
			//printf("Runnable:frame %d processed, move on!\n", i);
		    }

		    delete encodedFrame;
		    l.unlock();
		}
	    }
	}
	mLiveSource->stop();
    } catch(const runtime_error &rt) {
	printf("Runnable:%s\n", rt.what());
    }

}

void Runnable::setRunning(bool run)
{
    mStartJob = run;
}

bool Runnable::isRunning()
{
    return mStartJob;
}
