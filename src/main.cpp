#include "CameraUsbDevice.h"
#include "MonitorUsb.h"
#include "Logger.h"
#include "V4l2Control.h"
#include "V4l2LiveSource.h"
#include "ThreadPool.h"
#include "Util.h"
#include "H264ServerMediaSubsession.h"

static vector<CameraUsbDevice *> *mPtrCameraList = nullptr;
MonitorUsb *mMonitorUsb = nullptr;

void startUsbCameraMonitor ()
{
    mMonitorUsb = new MonitorUsb(mPtrCameraList);
    mMonitorUsb->startMonitor();
}



#include "liveMedia.hh"
#include "BasicUsageEnvironment.hh"
#include "GroupsockHelper.hh"

TaskScheduler *scheduler = nullptr;


// To make the second and subsequent client for each stream reuse the same
// input stream as the first client (rather than playing the file from the
// start for each client), change the following "False" to "True":
Boolean reuseFirstSource = False;

// To stream *only* MPEG-1 or 2 video "I" frames
// (e.g., to reduce network bandwidth),
// change the following "False" to "True":
Boolean iFramesOnly = False;

RTPSink* videoSink;

static void announceStream(RTSPServer* rtspServer, ServerMediaSession* sms,
	       char const* streamName) {
    char* url = rtspServer->rtspURL(sms);
    UsageEnvironment& env = rtspServer->envir();

    env << "Play this stream using the URL \"" << url << "\"\n";
    
    delete[] url;
}

ThreadPool* pool = nullptr;

void cameraEventHandler(int event_sig)
{
    printf("signal received: %d \n", event_sig);
/*    Runnable r(nullptr, nullptr);
    if(pool){
	pool->addTask(&r);
    }
*/
    switch(event_sig) {
	case SIGINT:
	case SIGTERM:
	    if (mMonitorUsb) {
		delete mMonitorUsb;
		mMonitorUsb = nullptr;
	    }
	    if (scheduler) {
		scheduler = nullptr;
		delete scheduler;
	    }
	    if (pool){
		pool = nullptr;
		delete pool;
	    }
	break;

	default:
	break;
    }

    exit(event_sig);
}


int main(int argc, char *argv[])
{

    struct sigaction sa;
    sa.sa_handler = cameraEventHandler;
    sa.sa_flags = 0;
    sigemptyset(&sa.sa_mask);
    sigaction(SIGUSR1, &sa, 0);
    sigaction(SIGINT, &sa, 0);
    sigaction(SIGTERM, &sa, 0);


    pool = new ThreadPool(4);

    /*pool->addTask(&run2);
    pool->addTask(&run3);
    pool->addTask(&run4);
*/


    scheduler = BasicTaskScheduler::createNew();
    UsageEnvironment *env = BasicUsageEnvironment::createNew(*scheduler);


    /*V4l2Control * v = NULL;
    try {
	v = new V4l2Control("/dev/video2");
    } catch (const runtime_error & e) {
	printf("error %s\n", e.what());
	return -1;
    }*/

    V4l2LiveParameters params;
    V4l2LiveSource  *inputSource = V4l2LiveSource::createNew(*env, params);
    StreamReplicator *streamReplicator = StreamReplicator::createNew(*env, inputSource, false);
    //create a source
    //FramedSource *source = streamReplicator->createStreamReplica();
    //FramedSource *videoSource = H264VideoStreamDiscreteFramer::createNew(*env, source);    



    struct in_addr destinationAddress;
    destinationAddress.s_addr = chooseRandomIPv4SSMAddress(*env);
    const unsigned short rtpPortNum = 18888;
    const unsigned short rtcpPortNum = rtpPortNum+1;
    const unsigned char ttl = 255;

    const Port rtpPort(rtpPortNum);
    const Port rtcpPort(rtcpPortNum);

    Groupsock rtpGroupsock(*env, destinationAddress, rtpPort, ttl);
    rtpGroupsock.multicastSendOnly(); // we're a SSM source
    Groupsock rtcpGroupsock(*env, destinationAddress, rtcpPort, ttl);
    rtcpGroupsock.multicastSendOnly(); // we're a SSM source

    OutPacketBuffer::maxSize = 614400;

    /*u_int8_t rtpPayloadformat = 96;//dynamic
    int height 	= 480; 
    int width 	= 640; 
    int depth	= 8; //(2^8 = 256 colors)
    const u_int8_t sps[] = {0x00, 0x00, 0x00, 0x01, 0x67, 0x7a, 0x00, 0x1e, 0xbc, 
			    0xb4, 0x05, 0x01, 0xed, 0x08, 0x00, 0x00, 0x03, 0x00, 
			    0x08, 0x00, 0x00, 0x03, 0x01, 0x94, 0x78, 0xb1, 0x75
			};
    const u_int8_t pps[] = {0x00, 0x00, 0x00, 0x01, 0x68, 0xce, 0x0f, 0xc8};
    RTPSink * videoSink = H264VideoRTPSink::createNew(*env, &rtcpGroupsock, rtpPayloadformat
		    ,sps, 27, pps, 8
		    );
    */
    // Create (and start) a 'RTCP instance' for this RTP sink:
    const unsigned estimatedSessionBandwidth = 500; // in kbps; for RTCP b/w share
    const unsigned maxCNAMElen = 100;
    unsigned char CNAME[maxCNAMElen+1];
    gethostname((char*)CNAME, maxCNAMElen);
    CNAME[maxCNAMElen] = '\0'; // just in case
    RTCPInstance* rtcp = RTCPInstance::createNew(*env, &rtcpGroupsock,
                            estimatedSessionBandwidth, CNAME,
                            videoSink, NULL /* we're a server */,
                            False /* we're a SSM source */);


    // Start Playing the Sink
    //videoSink->startPlaying(*videoSource, NULL, NULL);

    // Create the RTSP server:
    RTSPServer* rtspServer = RTSPServer::createNew(*env, 8554);
    if (rtspServer == NULL) {
	*env << "Failed to create RTSP server: " << env->getResultMsg() << "\n";
	exit(1);
    }

    char const* descriptionString	= "Session streamed by \" V4l2LiveSource stream\"";
    char const* streamName 		= "V4l2LiveSource";
    // Set up each of the possible streams that can be served by the
    // RTSP server.  Each such stream is implemented using a
    // "ServerMediaSession" object, plus one or more
    // "ServerMediaSubsession" objects for each audio/video substream.

    // NOTE: This *must* be a Program Stream; not an Elementary Stream
    ServerMediaSession* sms = ServerMediaSession::createNew(
				*env, 
				streamName, 
				streamName,
                		"Session streamed by \"V4l2LiveSource\"",
                        	True /*SSM*/
				);
    ServerMediaSubsession *sSession = NULL;
    sSession = H264ServerMediaSubsession::createNew(*env, streamReplicator, false);
    sms->addSubsession(sSession);
    //sms->addSubsession(PassiveServerMediaSubsession::createNew(*videoSink, rtcp));

    rtspServer->addServerMediaSession(sms);
    char* url = rtspServer->rtspURL(sms);

    announceStream(rtspServer, sms, streamName);

    Runnable run1(scheduler, inputSource), run2(scheduler, inputSource); 
    sleep(5);
    pool->addTask(&run1);

    env->taskScheduler().doEventLoop();

/*    delete scheduler;
    envir()->reclaim();
    env = NULL;
*/

/*	Logger l("Logger");

	mPtrCameraList = new vector<CameraUsbDevice *>();

	struct sigaction sa;
	sa.sa_handler = cameraEventHandler;
	sa.sa_flags = 0;
	sigemptyset(&sa.sa_mask);
	sigaction(SIGUSR1, &sa, 0);

	thread usbMonitor (startUsbCameraMonitor);

	usbMonitor.join();
*/
	return 0;
}

