#include "Logger.h"
#include "V4l2LiveSource.h"


#include <stdint.h>

EventTriggerId V4l2LiveSource::mEventTriggerId = 0;
deque<VideoFrame *> V4l2LiveSource::sVideoFramesQueue; 
deque<EncodedVideoFrame *> V4l2LiveSource::sEncodedVideoFramesQueue; 

V4l2LiveSource *V4l2LiveSource::createNew(UsageEnvironment &env, V4l2LiveParameters params)
{
    return new V4l2LiveSource(env, params);
}

V4l2LiveSource::V4l2LiveSource(UsageEnvironment &env, V4l2LiveParameters params)
		: FramedSource(env), fParams(params), processedFrame(false), mFrame(nullptr), 
		    clientDisconnected(false)
{
    if (mEventTriggerId == 0){
	mEventTriggerId = envir().taskScheduler().createEventTrigger(V4l2LiveSource::deliveFrame0);
    }
}


V4l2LiveSource::~V4l2LiveSource()
{
    envir().taskScheduler().deleteEventTrigger(mEventTriggerId);
    mEventTriggerId = 0;
}

void V4l2LiveSource::doGetNextFrame()
{
  // This function is called (by our 'downstream' object) when it asks for new data.
    printf("doGetNextFrame\n");
  // Note: If, for some reason, the source device stops being readable (e.g., it gets closed), then you do the following:
    if (clientDisconnected /* the source stops being readable, heck status, client disconnected */ /*%%% TO BE WRITTEN %%%*/) {
	printf("clientDisconnected, handle closure\n");
	handleClosure();
	return;
    }

  // If a new frame of data is immediately available to be delivered, then do this now:
    bool frame = !sVideoFramesQueue.empty();
    if ( frame/* a new frame of data is immediately available to be delivered*/ /*%%% TO BE WRITTEN %%%*/) {
	deliverFrame();
    }

  // No new data is immediately available to be delivered.  We don't do anything more here.
  // Instead, our event trigger must be called (e.g., from a separate thread) when new data becomes available.
}

void V4l2LiveSource::deliveFrame0(void *clientData)
{
    ((V4l2LiveSource*)clientData)->deliverFrame();
}

void V4l2LiveSource::deliverFrame()
{
  // This function is called when new frame data is available from the device.
  // We deliver this data by copying it to the 'downstream' object, using the following parameters (class members):
  // 'in' parameters (these should *not* be modified by this function):
  //     fTo: The frame data is copied to this address.
  //         (Note that the variable "fTo" is *not* modified.  Instead,
  //          the frame data is copied to the address pointed to by "fTo".)
  //     fMaxSize: This is the maximum number of bytes that can be copied
  //         (If the actual frame is larger than this, then it should
  //          be truncated, and "fNumTruncatedBytes" set accordingly.)
  // 'out' parameters (these are modified by this function):
  //     fFrameSize: Should be set to the delivered frame size (<= fMaxSize).
  //     fNumTruncatedBytes: Should be set iff the delivered frame would have been
  //         bigger than "fMaxSize", in which case it's set to the number of bytes
  //         that have been omitted.
  //     fPresentationTime: Should be set to the frame's presentation time
  //         (seconds, microseconds).  This time must be aligned with 'wall-clock time' - i.e., the time that you would get
  //         by calling "gettimeofday()".
  //     fDurationInMicroseconds: Should be set to the frame's duration, if known.
  //         If, however, the device is a 'live source' (e.g., encoded from a camera or microphone), then we probably don't need
  //         to set this variable, because - in this case - data will never arrive 'early'.
  // Note the code below.

    if (!isCurrentlyAwaitingData()) {
	printf("return, curently awaiting data!\n");
	return;
    }

    if (clientDisconnected) {
	printf("client got disconnected \n");
	return;
    }

    u_int8_t *newFrameDataStart = (u_int8_t*)0xDEADBEEF; //%%% TO BE WRITTEN %%%
    unsigned int newFrameSize = 0; //%%% TO BE WRITTEN %%%
    unsigned int truncSize = 0;

    timeval curTime;
    gettimeofday(&curTime, NULL);

    VideoFrame *mFrame = sVideoFramesQueue.front();
    sVideoFramesQueue.pop_front();


    newFrameDataStart = mFrame->getFrameData();
    newFrameSize = mFrame->getFrameSize();

    if (newFrameSize >= 4 && newFrameDataStart[0] == 0 && newFrameDataStart[1] == 0 &&
	((newFrameDataStart[2] == 0 && newFrameDataStart[3] == 1) || newFrameDataStart[2] == 1)) {
	printf("LiveSource:start code detected in nal, removed it\n");
	truncSize = 4;
    }

    newFrameDataStart = newFrameDataStart + truncSize;
    newFrameSize = newFrameSize - truncSize;

    if (newFrameSize > fMaxSize) {
	fFrameSize = fMaxSize;
	fNumTruncatedBytes = newFrameSize - fMaxSize;
    } else {
	fFrameSize = newFrameSize;
    }

    //gettimeofday(&fPresentationTime, NULL); // If you have a more accurate time - e.g., from an encoder - then use that instead.
    // If the device is *not* a 'live source' (e.g., it comes instead from a file or buffer), then set "fDurationInMicroseconds" here.
    //fPresentationTime = frame->getPresentationTime();
    printf("LiveSource:frameSize after checking:%d\n", fFrameSize);

    timeval diff;
    timeval here;
    timersub(&curTime, &here, &diff);
    fPresentationTime = here;
    memmove(fTo, newFrameDataStart, fFrameSize);
    processedFrame = true;

    delete mFrame;
    printf("Livesource:inform the reader that it is now available\n");
    FramedSource::afterGetting(this);
}

int V4l2LiveSource::getNewFrame()
{
    printf("V4l2LiveSource gotFrame\n");
    //processedFrame = false;
}

void V4l2LiveSource::setVideoFrame(VideoFrame *frame) {
    mFrame = frame;
}

void V4l2LiveSource::signalNewFrame(TaskScheduler *scheduler, V4l2LiveSource *liveSource)
{
    scheduler->triggerEvent(mEventTriggerId, liveSource);
}

