#include "CameraUsbDevice.h"
#include "MonitorUsb.h"
#include "V4l2Control.h"
#include "Logger.h"
#include "Util.h"

//bool?
void Util::getV4l2Device(const char *ueventPath, char *v4lDevice)
{
    string videoStr ("video4linux");
    if (ueventPath != nullptr) {
        string sys_path = "sys" + string (ueventPath);
        size_t found = sys_path.find(videoStr);

        if (found != string::npos) {
            char separator = '/';
            size_t i = sys_path.rfind(separator, sys_path.length());
            if (i != string::npos) {
                string device = sys_path.substr(i + 1, sys_path.length() - 1);

                device = "/dev/" + device;
                char cstr[device.size() + 1];
                device.copy(cstr, device.size() + 1);
                cstr[device.size()]='\0';
		if (v4lDevice)
		    strcpy(v4lDevice, cstr);
	    }
	}
    }
}


bool Util::isVideoCaptureDevice(const char *device, __u32 *capabilities)
{
    bool isCaptureDev = false;

    if (device != nullptr)
    {
	try {
	    V4l2Control v(device);
	    __u32 caps = v.getV4l2Capabilities();

	    if (caps & V4L2_CAP_VIDEO_CAPTURE) {
		isCaptureDev = true;
		* capabilities = caps;

		printf("%s is a video capture device\n", device);
	    } else {
		printf("%s is not a video capture device\n", device);
	    }
	} catch (runtime_error &rt) {
	    printf("%s\n", rt.what());
	}
    }

    return isCaptureDev;
}

//bool ?
int Util::readUsbAttributes(char *output, char *attribute, char *sysPath)
{
    char buff[256]      = {0};
    char full_path[256] = {0};
    int error		= -1;
    sprintf(full_path, "/sys%s/%s", sysPath, attribute);

    FILE * f = fopen(full_path,"r");
    if (f) {
        if ((fread(buff, 100, 1, f) > 0) || (feof(f))) {
            strcpy(output, buff);
        }
        error = fclose(f);
    }
    return error;
}


CameraUsbDevice *Util::getCameraInfo(char *path, const char *device, __u32 &caps)
{
    char *p;

    /* remove everything after video4linux */
    p = strstr(path, "video4linux");
    if (p) {
	*(p - 1) = '\0';
    }

    p = strrchr(path, '/');
    if (p) {
	*p = '\0';
    }

    /* Construct camera objects and extract information from sysfs */
    char manufacturer[CameraUsbDevice::MAXSIZE];
    char idProduct[CameraUsbDevice::MINSIZE];
    char idVendor[CameraUsbDevice::MINSIZE];
    char version[CameraUsbDevice::MINSIZE];
    char busnum[CameraUsbDevice::MINSIZE];
    char speed[CameraUsbDevice::MINSIZE];
    //check for errors ?
    readUsbAttributes(manufacturer, (char*)"manufacturer", path);
    readUsbAttributes(idVendor, (char*)"idVendor", path);
    readUsbAttributes(idProduct, (char*)"idProduct", path);
    readUsbAttributes(busnum, (char*)"busNum", path);
    readUsbAttributes(speed, (char*)"speed", path);
    readUsbAttributes(version, (char*)"version", path);

    CameraUsbDevice *cameraDevice = new CameraUsbDevice(device, idProduct, atoi(busnum), idVendor,
						atoi(speed), version, manufacturer, caps
						);

    return cameraDevice;
}
