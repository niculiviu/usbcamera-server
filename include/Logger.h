#ifndef _LOGGER_H_
#define _LOGGER_H_

#include <syslog.h>
#include <string.h>
#include <stdio.h>

/**
 * simple wrapper over syslog
 * three types of log severity(Error, Info and Debug)
 */

class Logger
{

public:
	static const int DEBUG;
	static const int ERROR;
	static const int INFO;

	void log(const int logPriority, const char *message, ...);

	Logger(const char *logName);

	~Logger();
};

#endif // _LOGGER_H_
