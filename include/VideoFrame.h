#ifndef _VIDEOFRAME_H_
#define _VIDEOFRAME_H_

#include <sys/time.h>

//make it c++ style
#define MAX_NALS 32

class VideoFrame {

protected:
    unsigned char *mFrameBuff;
    unsigned int mFrameSize;
    timeval mTimeStamp;

public:
    VideoFrame();
    VideoFrame(unsigned int frameSize);
    virtual ~VideoFrame();

    void setPresentationTime(timeval t) {mTimeStamp = t;};
    timeval getPresentationTime() {return mTimeStamp;};    
    //void setFrameSize(unsigned int frameSize) {mFrameSize = frameSize;};
    unsigned int getFrameSize() {return mFrameSize;};
    unsigned char *getFrameData() {return mFrameBuff;};

};

/**
* nal unit
*/

class Nal {

private:
    unsigned char *nalPayload;
    unsigned int nalPayloadSize;

public:
    Nal();
    unsigned char *getNal() {return nalPayload;};
    unsigned int getNalSize() {return nalPayloadSize;};
    void setNal(unsigned char *p) {nalPayload = p;};
    void setNalSize(int size) {nalPayloadSize = size;};

};

/**
* encoded video frame
*/

class EncodedVideoFrame : public VideoFrame {
    
private:
    int currentNal;
    Nal nals[MAX_NALS];

public:
    EncodedVideoFrame();
    virtual ~EncodedVideoFrame();
    bool setNal(unsigned char *nal, unsigned int nalSize);
    int getTotalNals() {return currentNal;};
    Nal *getNals() {return nals;};

};


#endif // _VIDEOFRAME_H_
