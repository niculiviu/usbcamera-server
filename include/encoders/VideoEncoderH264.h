/**
*
*
*/

#ifndef _VIDEO_ENCODER_H264_H
#define _VIDEO_ENCODER_H264_H

#include <stdexcept>
#include <stdint.h>
#include <stdio.h>

extern "C" {
#include <x264.h>
#include <libswscale/swscale.h>
#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
}

#include <linux/videodev2.h>
#include <string.h>
#include "../VideoFrame.h"

using std::runtime_error;

class VideoEncoderH264 {

private:
    int fps;
    int inPts;
    int outPts;
    int dts;
    int piNal;

    x264_picture_t picIn;
    x264_picture_t picOut;
    x264_param_t mParam;
    x264_t *encoder;
    x264_nal_t *nals;

    struct SwsContext *sws;
    AVPixelFormat in_pixel_format, out_pixel_format;
    AVFrame             *inFrame, *outFrame;
    FILE *pFile;

public:
    VideoEncoderH264();
    ~VideoEncoderH264();

    EncodedVideoFrame *encodeFrame(unsigned char *rawFrame, unsigned int rawFrameSize);
    bool encodeHeadersFrame();
};

#endif //_VIDEO_ENCODER_H264_H