#ifndef _UTIL_H_
#define _UTIL_H_

#include "CameraUsbDevice.h"

using std::string;
using std::runtime_error;

/**
 * Utility class
*/

class Util
{
    public:

	/**
         * Gets the connected video device
         *
         * @param ueventPath the uevent path returned by the libevent mechanism
         * @param v4lDevice the current device attached
         *
	*/
	static void getV4l2Device(const char *ueventPath, char *v4lDevice);

	/**
	 * Checks if the device is a video capturing one
	 *
	 * @param device video4linux device
	 * @param capabilities v4l2 device capabilities
	 *
	 * @retval true is this device has video capturing capabilities
	 * @retval false
	 *
        */
	static bool isVideoCaptureDevice(const char *device, __u32 *capabilities);

	/** 
	 * Constructs a CameraUsbDevice object
	 *
	 * @param path the path representing an uevent exported in sysfs
	 * @device the device that suports video4Linux api
	 * @caps the v4l2 device capabilities
	 *
	 * @retval ptr to a CameraUsbDevice object that contains:
	 * the device path from where to read video data;
	 * usb info e.g: idVendor, idProduct, busNum, speed etc
	 * v4l2 capabilities
	*/
	static CameraUsbDevice *getCameraInfo(char *path, const char *device, __u32 &caps);


	/**
	 * Gets the usb device properties from sysfs
	*/
	static int readUsbAttributes(char *output, char *attribute, char *sysPath); 

};

#endif // _UTIL_H_