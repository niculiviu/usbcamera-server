#ifndef _V4L2CONTROL_H_
#define _V4L2CONTROL_H_

#include <errno.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <stdexcept>
#include <linux/videodev2.h>
#include <unistd.h>

using std::runtime_error;

/**
 * wrapper over some V4L(Video for Linux API) and
 * import some of the v4l2-ctl application functionality.
 *
 * To grab a video frame the following steps should be done:
 *    - open a descriptor to the video device
 *    - retrieve and analyze the video device capabilities
 *    - set the capture format(YUV, RGB, MJPEG, etc)
 *    - inform the video device about your buffers(buffer request). 
 *      submit a buffer to the device(queue) and retrieve it once it's full
 *      with data(dequeue).
 *    - set the buffer size and frame start offset in memory by creating a new memory
 *      mapping for it.
 *    - put the device into streaming mode
 *    - keep queuing/dequeuing the buffers repeatedly. Every call will bring
 *      a new frame. The delay between each frame represents the FPS(frame per second)rate
 *    - turn off streaming mode and close the video device
 *
*/

struct buffer{
    void   *data;
    size_t  length;
};


class V4l2Control
{

public:
    int mFd;

    V4l2Control(const char *device);
    ~V4l2Control();

    bool configure(/*width, height, format, etc*/);
    unsigned int getV4l2Capabilities(void);
    v4l2_fmtdesc getVideoFormats(__u32 type);
    int getV4l2Fd(void);
    unsigned int getBufferImageSize(void);
    unsigned int getBufferBytesUsed(void);

    bool getFrame(unsigned char *frame);

private:

    struct v4l2_format mFmt;

    bool setVideoFormat(void);
    bool initMap(void);

    bool startCapturing(void);
    bool stopCapturing(void);

    /* buffers to be allocated */
    unsigned int mNrBuffers = 0;
    struct buffer* buffers;

    unsigned int mBytesUsed = 0;

    bool readFrame(unsigned char *frame);

};


#endif // _V4L2CONTROL_H_
