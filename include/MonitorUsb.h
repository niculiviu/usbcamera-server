#ifndef _MONITORUSB_H_
#define _MONITORUSB_H_

#include "CameraUsbDevice.h"

#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

#include <linux/netlink.h>
#include <linux/videodev2.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include <event2/bufferevent.h>
#include <event2/buffer.h>
#include <event2/util.h>
#include <event2/event.h>

#include <iostream>
#include <string>
#include <vector>

using std::vector;

/**
 * uses libevent API and executes a callback function over a file descriptor that is an uevent socket
 * creates a bufferevent over an uevent socket and registers a callback function
 */

class MonitorUsb
{

private:
	static int mSock;
	static bufferevent *mBufevent;
	static std::vector<CameraUsbDevice *> *mPtrDevicesList;


public:
	MonitorUsb(std::vector<CameraUsbDevice *> *dev);

	int startMonitor(void);
	int readUsbAttributes(char *output, char *attribute, char *sysPath);
	static void readcb(struct bufferevent *bev, void *ctx);
	static void eventcb(struct bufferevent *bev, short events, void *ctx);

	~MonitorUsb();
};

#endif // _MONITORUSB_H_

