#ifndef _RUNNABLE_H_
#define _RUNNABLE_H_

#include "V4l2LiveSource.h"
#include "encoders/VideoEncoderH264.h"
#include <liveMedia.hh>
#include <condition_variable>

using std::condition_variable;
using std::mutex;

class Runnable
{

public:
    void job(unsigned int id);

    /**
     * Sets the running flag
    */
    void setRunning(bool run);


    /**
     * Returns the value of the running flag
     * @return True if the runnable is running. Flase otherwise
    */
    bool isRunning();


    Runnable(TaskScheduler *scheduler, V4l2LiveSource *liveSource);
    ~Runnable();

private:
    TaskScheduler *mScheduler;
    V4l2LiveSource *mLiveSource;
    mutex mtx;
    condition_variable cv;
    unsigned int id;
    bool mStartJob;
};

#endif // RUNNABLE_H_
