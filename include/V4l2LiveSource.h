#ifndef _V4L2_LIVESOURCE_H_
#define _V4L2_LIVESOURCE_H_

#include <deque>
#include <mutex>
#include <condition_variable>

#ifndef _FRAMED_SOURCE_HH
#include "FramedSource.hh"
#endif

#include "VideoFrame.h"

using std::deque;

extern "C" {
#include <x264.h>
}

/**
 * Template from live555 media source 
 *
 */


/**
 * to define specific encoder parameters
*/
class V4l2LiveParameters {

};

class V4l2LiveSource : public FramedSource {

    public:
	static V4l2LiveSource *createNew(UsageEnvironment &env, V4l2LiveParameters params);
	static EventTriggerId mEventTriggerId;
	static void signalNewFrame(TaskScheduler *scheduler, V4l2LiveSource *liveSource);
	static deque<VideoFrame *> sVideoFramesQueue;
	static deque<EncodedVideoFrame *> sEncodedVideoFramesQueue;
	void setVideoFrame(VideoFrame *frame);
	void stop() {clientDisconnected = true;};
	void setConsumed(bool consumed) {processedFrame = consumed;};
	bool wasConsumed(){return processedFrame;}

    protected:
	V4l2LiveSource(UsageEnvironment &env, V4l2LiveParameters params); 
	virtual ~V4l2LiveSource();

    protected:
	bool processedFrame;
	bool clientDisconnected;

    private:
	VideoFrame *mFrame;
	virtual void doGetNextFrame();
	static void deliveFrame0(void *clientData);
	void deliverFrame();
	V4l2LiveParameters fParams;
	int getNewFrame(void);
};

#endif //  _V4L2_LIVESOURCE_H_
