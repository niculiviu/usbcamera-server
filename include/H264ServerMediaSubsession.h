#ifndef _H264_SERVER_MEDIA_SUBSESSION_H_
#define _H264_SERVER_MEDIA_SUBSESSION_H_

#include <liveMedia.hh>

class H264ServerMediaSubsession : public OnDemandServerMediaSubsession {

public:
    static H264ServerMediaSubsession *createNew(UsageEnvironment &env, StreamReplicator *replica, Boolean reuseFirstSource);

protected:
    H264ServerMediaSubsession(UsageEnvironment &env, StreamReplicator *replica, Boolean reuseFirstSource);
    FramedSource *createNewStreamSource(unsigned int clientSessionId, unsigned int &estBitrate);
    RTPSink *createNewRTPSink(Groupsock *rtpGroupsock, unsigned char rtpPayloadFormat, FramedSource *inputSource);
    RTCPInstance *creanteRTCP(Groupsock* RTCPgs, unsigned totSessionBW, /* in kbps */
				unsigned char const* cname, RTPSink* sink);

    StreamReplicator* replicator;

    virtual ~H264ServerMediaSubsession();
};

#endif // _H264_SERVER_MEDIA_SUBSESSION_H_
