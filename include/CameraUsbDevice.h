#ifndef _CAMERA_USBDEVICE_H_
#define _CAMERA_USBDEVICE_H_

#include <asm/types.h>
#include <list>

using std::list;

/**
 * camera usb device properties
 *
 */

class CameraUsbDevice
{

public:

	static const int MINSIZE = 10;
	static const int MAXSIZE = 256;


	CameraUsbDevice(const char *devicePath, char *idProduct, int busnum, char *idVendor,
		int speed, char *version, char *manufacturer, __u32 &device_capabilities
		);


	void getDevicePath(char *out) const;
	void getIdProduct(char *out) const;
	int getBusNum (void) const;
	void getIdVendor (char *output) const;
	int getSpeed (void) const;
	void getVersion (char *version_out) const;
	void getManufacturer (char *man_out) const;
	__u32 getDeviceCapabilities(void);
	void getCameraUsbDeviceList(std::list<CameraUsbDevice *> &list_out)const;

	~CameraUsbDevice();

private:

	__u32 mDeviceCap	= 0;
	int mBusNum		= 0;
	int mSpeed		= 0;
	char mDevicePath[MAXSIZE]={0};
	char mIdProduct[MINSIZE]={0};
	char mIdVendor[MINSIZE]	={0};
	char mVersion[MINSIZE]  ={0};
	char mManufacturer[MAXSIZE]={0};
};

#endif // _CAMERA_USBDEVICE_H_

