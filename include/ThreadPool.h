#ifndef _THREAD_POOL_H_
#define _THREAD_POOL_H_

#include <condition_variable>
#include <deque>
#include <mutex>
#include <thread>
#include <vector>

#include "Runnable.h"

using std::condition_variable;
using std::deque;
using std::mutex;
using std::thread;
using std::vector;
using std::unique_lock;
using std::lock_guard;

class TaskQueue
{

public:
    TaskQueue();
    void enqueue(Runnable *r);
    void dequeue();
    void resetIterator();
    void clear();
    Runnable* getCurrent();
    void next();
    bool isEmpty();

private:
    deque<Runnable *> 		queue;
    deque<Runnable *>::iterator	iter;

};

class ThreadPool
{

public:
    ThreadPool(unsigned int threads = 0);
    ~ThreadPool();

    void addTask(Runnable * const task);
    void removeTask();
    void stop();

private:
    TaskQueue		mQueue;
    vector<thread>	workers;
    mutex		m_mutex;
    condition_variable	cv;
    bool		run;

    void worker_thread(unsigned int i);
};

#endif // __THREAD_POOL_H