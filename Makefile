#TODO: add 
#-D ALLOW_RTSP_SERVER_PORT_REUSE=1
#to the "COMPILE_OPTS =" line of your "config." file
#
#add documentation for sysroot e.g raspberry pi
#add debug flag -v, to be activated at build time
#
ifndef CXXFLAGS
CXXFLAGS += -std=c++11 -Wall -Wextra -g -Werror=format-security --sysroot=/ 
#-v
endif

INCLUDES = \
	-Iinclude/ 					\
	-I=usr/include/libusb-1.0/ 			\
	-I=usr/include/event2/ 				\
	-I=usr/local/include/BasicUsageEnvironment	\
	-I=usr/local/include/groupsock			\
	-I=usr/local/include/liveMedia			\
	-I=usr/local/include/UsageEnvironment		\
	-I=usr/local/lib				\

# TODO:x264.h is located in /usr/include. This should be grouped under a specific folder
# e.g /usr/include/x264/

SRC_PATH = src
BUILD_PATH = build
BIN_PATH = $(BUILD_PATH)/bin
LIVE555_LIBS_PATH = usr/local/lib

BIN_NAME = usbCamera-server

SOURCES = $(shell find $(SRC_PATH) -name '*.cpp')
OBJECTS = $(SOURCES:$(SRC_PATH)/%.cpp=$(BUILD_PATH)/%.o)
DEPS = $(OBJECTS:.o=.d)

#TODO:compile x264 with --enabled-shared, use stable branch and install library properly
#libyuv was compiled and .so file copied manually into /usr/lib/x86_64-linux-gnu/
#when in yocto, all the libraries should be nicely grouped into specific directories
# libyuv depends o libjpeg.so
LDFLAGS += \
	-lusb-1.0		\
	-levent			\
	-lpthread		\
	-Lusr/local/lib		\
	-lx264			\
	-lswscale		\
	-lavcodec		\
	-lavutil		\

# live555 requires circular linking
LIVE555_LIBS += \
	-L${LIVE555_LIBS_PATH} -lBasicUsageEnvironment 	\
	-L${LIVE555_LIBS_PATH} -lUsageEnvironment		\
	-L${LIVE555_LIBS_PATH} -lliveMedia			\
	-L${LIVE555_LIBS_PATH} -lgroupsock			\

.PHONY: usbCamera-server
usbCamera-server: release

.PHONY: release
release: export CXXFLAGS := $(CXXFLAGS)
release: dirs
	@$(MAKE) all

.PHONY: dirs
dirs:
	@echo "Creating objects directories"
	@mkdir -p $(dir $(OBJECTS))
	@mkdir -p $(BIN_PATH)

.PHONY: clean
clean:
	@$(RM) $(BIN_NAME)
	@echo "Build deleted"
	@$(RM) -r $(BUILD_PATH)
	@$(RM) -r $(BIN_PATH)

.PHONY: all
all: $(BIN_PATH)/$(BIN_NAME)
	@$(RM) $(BIN_NAME)

$(BIN_PATH)/$(BIN_NAME): $(OBJECTS)
	@echo "Linking: $@"
	$(CXX) $(OBJECTS) $(LDFLAGS) ${LIVE555_LIBS} -o $@ ${LIVE555_LIBS}

-include $(DEPS)

$(BUILD_PATH)/%.o: $(SRC_PATH)/%.cpp
	@echo "Compiling: $< -> $@"
	$(CXX) $(CXXFLAGS) $(INCLUDES) -MP -MMD -c $< -o $@

